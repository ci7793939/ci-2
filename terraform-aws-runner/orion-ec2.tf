provider "aws" {
  region     = "us-west-2"
    profile = "AWS-cli-Access"
 shared_credentials_files = ["~/.aws/credentials"]
}

# AMI from data source
data "aws_ami" "app_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

# Security Group
resource "aws_security_group" "allow_ssh_http_https" {
  name        = "orion-sg"
  description = "Allow http and https inbound traffic"
  ingress {
    description = "https from vpc"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "http from vpc"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ssh from vpc"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#EC2 + Security group reference
resource "aws_instance" "orion-ec2" {
  ami                    = data.aws_ami.app_ami.id
  instance_type          = var.instancetype
  tags                   = var.aws_common_tag
  vpc_security_group_ids = ["${aws_security_group.allow_ssh_http_https.id}"]
  key_name               = "TF_key"


  provisioner "remote-exec" {
    inline = [
      "sudo yum install -y docker",
      "sudo usermod -a -G docker ec2-user",
      "sudo service docker start",
      "sudo systemctl enable docker.service",
      "sudo mkdir -p /data/gitlab/runner/",
      "export TOKEN=${var.token}",
      "sudo docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v /srv/gitlab-runner/config:/etc/gitlab-runner  gitlab/gitlab-runner:latest",
      "sudo docker run --rm -v /data/gitlab/runner/:/etc/gitlab-runner gitlab/gitlab-runner register --non-interactive --executor 'docker' --docker-image docker:dind --url 'https://gitlab.com/' --registration-token $TOKEN --description 'docker-runner-orion' --tag-list 'docker-orion' --run-untagged='true' --locked='false' --access-level='not_protected' --docker-privileged --docker-volumes '/var/run/docker.sock:/var/run/docker.sock'",
      "gitlab-runner start",
      "sudo systemctl status docker.service",
    ]

  }

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file("tfkey")
    host        = self.public_ip
  }
}

resource "aws_key_pair" "TF_key" {
  key_name   = "TF_key"
  public_key = tls_private_key.rsa.public_key_openssh
}

# RSA key of size 4096 bits
resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "TF-key" {
  content  = tls_private_key.rsa.private_key_pem
  filename = "tfkey"
}


