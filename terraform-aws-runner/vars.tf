variable "instancetype" {
  type        = string
  description = "instance type"
  default     = "t2.nano"
}

variable "aws_common_tag" {
  type        = map(any)
  description = "aws tag"
  default = {
    name = "ec2"
  }
}

variable token {
  type        = string
  default     = "GR13489416JqSxDYtn-HgLdoMZU1f"
  description = "description"
}
