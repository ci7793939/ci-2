terraform {
  backend "s3" {
    bucket     = "site-bucket-orion"
    key        = "gitlab-ci-runner/terraform.tfstate"
    shared_credentials_file = "~/.aws/credentials"
    region     = "us-west-2"
    profile = "AWS-cli-Access"
  }

}