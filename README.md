# jobs en parallèle
- Pour avoir des jobs qui s'exécutent en parrallèle, il doivent avoir le même stage (le même nom)

job-1:
  stage: check
  tags:
  - docker-orion

job-2:
  stage: check
  tags:
  - docker-orion

avantages: gain de temps

# artefacts
- Permet de stocker un livrable, qui sera utilisé par un autre job

job-1:
  stage: package
  tags:
  - docker-orion
script:
 - mvn package # génère le app.jar et le stock en tantque artifact
artifacts:
  paths:
   - app.jar
expire_in: 1 week

job-2:
  stage: build-image
  tags:
  - docker-orion 
script:
 - docker build . # build l'image docker contenant l'app.jar

 # cache
 - la notion de cache est utilisée pour stocker des dépendances dont on a besoin constament lors du build, ça permet d'éviter les téléchagements repétés. Il peut être defini au niveau global ou spécifique à un job


- le job-a crée un dossier vendor et stocke un fichier hello.txt comme cache avec pour  clée d'accès build-cache

job-a:
  stage: build-image
  script:
    - mkdir vendor/
    - echo "build" > vendor/hello.txt
  cache:
    key: build-cache
    patch:
     - vendor/
  after_script:
     - echo "world"

  - le job-b utilise la clée build-cache pour accéder au cache et afficher le contenu du fichier

 job-b:
  stage: test
  script:
    - cat  vendor/hello.txt
  cache:
    key: build-cache
  
 # cache vs artefacts
 - Les deux permettent de stocker des données
 - L'artefact stocke des livrables (.jar, .war, ...) produit lors du build
 - le cache stocke des dépendances reutilisables, necessaire et indispensable à la bonne exécution du build

 https://docs.gitlab.com/ee/user/packages/container_registry/build_and_push_images.html




